#!/usr/bin/env perl
#===============================================================================
#
#         FILE: syntax.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2012-12-30 15:58
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::Strict;

$Test::Strict::TEST_SYNTAX = 1;
$Test::Strict::TEST_STRICT = 1;
$Test::Strict::TEST_WARNINGS = 1;

all_perl_files_ok;

