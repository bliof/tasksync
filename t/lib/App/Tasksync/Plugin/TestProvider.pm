#===============================================================================
#
#         FILE: TestProvider.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-12 00:06
#     REVISION: ---
#===============================================================================

package App::Tasksync::Plugin::TestProvider;

use strict;
use warnings;

sub new {
    my $class = shift;
    my $config = shift || {};
    my $self = {%$config};
    bless ($self, $class);
    return $self;
}

sub alias {
    return 'test_provider';
}

1;

