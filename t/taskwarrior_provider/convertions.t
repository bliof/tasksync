#!/usr/bin/env perl
#===============================================================================
#
#         FILE: convertions.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-16 17:06
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;
use App::Tasksync::Plugin::TaskwarriorProvider;

my $provider = App::Tasksync::Plugin::TaskwarriorProvider->new({ alias => 'test_provider' });

subtest 'format_warrior_date' => sub {
    my $date = $provider->format_warrior_date(1361027733);
    cmp_ok $date, 'eq', '20130216T171533Z';
};

subtest 'parse_warrior_date' => sub {
    my $date = $provider->parse_warrior_date('20130216T173956Z');
    cmp_ok $date, 'eq', '1361029196';
};

done_testing;

