#!/usr/bin/env perl
#===============================================================================
#
#         FILE: utils.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-11 23:46
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;

use App::Tasksync::Utils qw(
    format_plugin_name
);

subtest 'format_plugin_name' => sub {
    subtest 'without project prefix' => sub {
        my $class = format_plugin_name 'Ninja';

        cmp_ok $class, 'eq', 'App::Tasksync::Plugin::Ninja';
    };

    subtest 'with project prefix' => sub {
        my $class = format_plugin_name 'App::Tasksync::Plugin::Boom';

        cmp_ok $class, 'eq', 'App::Tasksync::Plugin::Boom';
    };

    subtest 'custom plugin name' => sub {
        my $class = format_plugin_name '+The::Little::Ninja::Provider';

        cmp_ok $class, 'eq', 'The::Little::Ninja::Provider';
    };
};

done_testing;

