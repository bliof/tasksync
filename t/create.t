#!/usr/bin/env perl
#===============================================================================
#
#         FILE: create.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-12 00:07
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;
use App::Tasksync;

subtest 'new' => sub {
    subtest 'with config' => sub {
        my $app = App::Tasksync->new(
            config => {
                providers => {
                    first => {
                        class => 'TestProvider'
                    },
                    second => {
                        class => 'TestProvider'
                    }
                }
            }
        );

        my $first = $app->get_provider('first');
        my $second = $app->get_provider('second');

        isa_ok $first, 'App::Tasksync::Plugin::TestProvider';
        isa_ok $second, 'App::Tasksync::Plugin::TestProvider';

        ok($first ne $second, 'two providers with the same class are not the same object');
    };

    subtest 'without config' => sub {
        my $app = App::Tasksync->new();

        my $provider = $app->get_provider('test_provider');

        isa_ok $provider, 'App::Tasksync::Plugin::TestProvider';
    };
};

done_testing;

