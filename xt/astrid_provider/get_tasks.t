#!/usr/bin/env perl
#===============================================================================
#
#         FILE: get_tasks.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-13 00:59
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;

use App::Tasksync;
use App::Tasksync::Task::Tag;
use List::MoreUtils qw(any);

my $app = App::Tasksync->new(config_file => 'tasksync.yaml');

my $provider = $app->get_provider('astrid_test_provider');

my $tag = App::Tasksync::Task::Tag->new({ name => 'Home' });

my $tasks_it = $provider->get_tasks({ tag => $tag });

while (my $task = $tasks_it->()) {
    diag explain $task;
    ok(
        any(sub { $_->{name} eq 'Home' }, @{ $task->tags }),
        $task->id_for('astrid_test_provider') . ' has tag Home'
    );
}

done_testing;

