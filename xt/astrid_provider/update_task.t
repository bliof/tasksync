#!/usr/bin/env perl
#===============================================================================
#
#         FILE: update_task.t
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-15 23:36
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;

use App::Tasksync;
use App::Tasksync::Task::Tag;
use List::MoreUtils qw(any);

my $app = App::Tasksync->new(config_file => 'tasksync.yaml');

my $provider = $app->get_provider('astrid_test_provider');

my $now = time;

subtest 'insert a task' => sub {
    my $task = App::Tasksync::Task->new(
        {
            title      => "I have a realy cool task #$now",
            created_at => $now,
        }
    );

    ok($provider->update($task), 'inserts');

    $provider->load_tasks({ force => 1 });

    my $inserted_task = $provider->find_task($task);

    diag explain $inserted_task;

    ok($inserted_task, 'can find the inserted task');
};

subtest 'update the task' => sub {
    my $task = App::Tasksync::Task->new(
        {
            title      => "Updated #$now",
            created_at => $now,
        }
    );

    ok($provider->update($task), 'updates');

    $provider->load_tasks({ force => 1 });

    my $updated_task = $provider->find_task($task);

    diag explain $updated_task;
    ok($updated_task, 'can update old task');
};

done_testing;

