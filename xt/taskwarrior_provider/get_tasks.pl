#!/usr/bin/env perl
#===============================================================================
#
#         FILE: get_tasks.pl
#
#        USAGE: ./get_tasks
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-16 19:24
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use strict;
use warnings;

use Data::Dumper;
use App::Tasksync;
use App::Tasksync::Task::Tag;

my $app = App::Tasksync->new();

my $provider = $app->get_provider('task');

#my $tag = App::Tasksync::Task::Tag->new({ name => 'Home' });

#my $tasks_it = $provider->get_tasks({ tag => $tag });
my $tasks_it = $provider->get_tasks();

while (my $task = $tasks_it->()) {
    print STDERR Dumper($task);
}

