#!/usr/bin/env perl
#===============================================================================
#
#         FILE: tasksync.pl
#
#        USAGE: ./tasksync
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-01-05 14:47
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use App::Tasksync;
use Getopt::Long;

main: {
    my ($show_help, $from, $to, $config_file, $check_only);
    my $ok = GetOptions(
        'help|h'       => \$show_help,
        'from|f=s'     => \$from,
        'to|t=s'       => \$to,
        'config|c=s'   => \$config_file,
        'only_check|o' => \$check_only
    );

    unless ($ok) {
        print_help();
        exit 1;
    }

    my $app = App::Tasksync->new(config_file => $config_file);

    if ($check_only) {
        my $result = $app->has_tasks_to_sync(
            from => $from,
            to => $to
        );
        if ($result) {
            print "There are tasks that could be synced\n";
        } else {
            print "Cannot find any task for syncing\n";
        }
        exit($result ? 0 : 1);
    } else {
        my $result = $app->sync(
            from => $from,
            to => $to
        );
        exit($result ? 0 : 1);
    }
}

sub print_help {
    print <<HELP;
tasksync
    -f|--from
    -t|--to
    -c|--config
    -h|--help
HELP
}

