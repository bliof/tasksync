#===============================================================================
#
#         FILE: Task.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-10 15:46
#     REVISION: ---
#===============================================================================

package App::Tasksync::Task;

use strict;
use warnings;

use Class::Accessor 'moose-like';

has ids         => ( is => 'rw' );
has title       => ( is => 'rw' );
has description => ( is => 'rw' );
has comments    => ( is => 'rw' );
has created_at  => ( is => 'rw' );
has updated_at  => ( is => 'rw' );
has due_date    => ( is => 'rw' );
has tags        => ( is => 'rw' );
has repeat      => ( is => 'rw' );
has importance  => ( is => 'rw' );

sub id_for {
    my $self = shift;
    my $provider = shift;

    $self->{ids} ||= {};

    return $self->ids->{$provider};
}

1;

