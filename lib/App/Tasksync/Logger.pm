#===============================================================================
#
#         FILE: Logger.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-16 00:28
#     REVISION: ---
#===============================================================================

package App::Tasksync::Logger;

use strict;
use warnings;
use Data::Dump qw(dump);

use Exporter;
our @EXPORT = qw(debug error warn);
our @ISA = ('Exporter');

sub debug {
    print 'debug: ', ninja_dump(@_), "\n";
}

sub error {
    print STDERR 'error: ', ninja_dump(@_), "\n";
}

sub warn {
    print 'warn: ', ninja_dump(@_), "\n";
}

sub ninja_dump {
    my $result = '';
    for my $current (@_) {
        if (ref $current) {
            $result .= dump $current;
            $result .= "\n";
        } else {
            $result .= $current . ' ';
        }
    }
    return $result;
}

1;

