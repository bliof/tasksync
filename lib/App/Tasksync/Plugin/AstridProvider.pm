#===============================================================================
#
#         FILE: AstridProvider.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-12 22:19
#     REVISION: ---
#===============================================================================

package App::Tasksync::Plugin::AstridProvider;

use strict;
use warnings;
use Astrid::Connection;

use List::MoreUtils qw(all);
use List::Util qw(first);
use POSIX qw(mktime);
use Time::Local;

use App::Tasksync::Logger;
use App::Tasksync::Task;
use App::Tasksync::Task::Tag;

my %IMPORTANCE = (
    critical => 0,
    high     => 1,
    normal   => 2,
    none     => 3
);

sub to_utc_time {
    my $time = shift or return undef;

    return mktime gmtime $time;
}

sub to_local_time {
    my $utc_time = shift or return undef;

    return timegm localtime $utc_time;
}

sub new {
    my $class = shift;
    my $config = shift || {};

    unless (all { $_ } @$config{qw/alias app_id secret username password/}) {
        die 'missing config parameter';
    }

    my $astrid = Astrid::Connection->new(app_id => $config->{app_id}, secret => $config->{secret});

    my $result = $astrid->sign_in($config->{username}, $config->{password});

    unless ($result->{status} eq 'success') {
        die 'cannot login to astrid';
    }

    my $self = {
        astrid => $astrid,
        alias  => $config->{alias}
    };
    bless($self, $class);

    return $self;
}

sub build_task {
    my $self = shift;
    my $astrid_task = shift or return undef;

    my $task = App::Tasksync::Task->new(
        {
            ids         => { $self->{alias} => $astrid_task->{id} },
            title       => $astrid_task->{title},
            description => $astrid_task->{notes},
            comments    => [],
            created_at  => to_utc_time($astrid_task->{created_at}),
            updated_at  => to_utc_time($astrid_task->{updated_at}),
            due_date    => to_utc_time($astrid_task->{due}),
            tags        => $self->build_tags($astrid_task->{tags}),
            repeat      => $astrid_task->{repeat},
            importance  => $self->to_tasksync_importance($astrid_task->{importance})
        }
    );

    return $task;
}

sub task_to_hash {
    my $self = shift;
    my $task = shift;

    my $result = {
        id         => $task->id_for($self->{alias}),
        title      => $task->title(),
        notes      => $task->description(),
        created_at => to_local_time($task->created_at()),
        updated_at => to_local_time($task->updated_at()),
        due        => to_local_time($task->due_date()),
        repeat     => $task->repeat(),
        tags       => [map { $_->name } @{ $task->tags() || [] }],
        importance => $self->to_astrid_importance($task->importance)
    };

    return $result;
}

sub build_tags {
    my $self = shift;
    my $tags = shift or return [];

    return [map { $self->build_tag($_) } @$tags];
}

sub build_tag {
    my $self = shift;
    my $astrid_tag = shift or return undef;

    my $tag = App::Tasksync::Task::Tag->new(
        {
            ids  => { $self->{alias} => $astrid_tag->{id} },
            name => $astrid_tag->{name}
        }
    );

    return $tag;
}

sub to_astrid_importance {
    my $self = shift;
    my $importance = shift;

    if (defined $importance and $importance =~ tr/0123/3210/) {
        return $importance;
    }

    return $IMPORTANCE{normal};
}

sub to_tasksync_importance {
    my $self = shift;
    my $importance = shift;

    if (defined $importance and $importance =~ tr/3210/0123/) {
        return $importance;
    }

    return;
}

sub load_tasks {
    my $self = shift;
    my $options = shift || {};

    if ($options->{force} or !$self->{tasks}) {
        my $selectors = $self->prepare_selectors($options);
        my $result    = $self->{astrid}->task_list(%$selectors);

        die 'cannot fetch tasks from astrid server' unless $result->{status} eq 'success';

        $self->{tasks} = $result->{list};
    }
}

sub load_tags {
    my $self = shift;
    my $options = shift || {};

    if ($options->{force} or !$self->{tags}) {
        my $result = $self->{astrid}->tag_list();

        die 'cannot fetch tags from astrid server' unless $result->{status} eq 'success';

        $self->{tags} = $result->{list};
    }
}

sub get_tasks {
    my $self    = shift;
    my $options = shift;

    $self->load_tasks($options);

    my $current = 0;

    return sub {
        if ($self->{tasks}->[$current]) {
            my $result = $self->build_task($self->{tasks}->[$current]);
            $current++;
            return $result;
        }
        return undef;
    };
}

sub prepare_selectors {
    my $self  = shift;
    my $query = shift;

    my %result;

    if ($query->{tag}) {
        my $tag = $query->{tag};

        unless ($tag->id_for($self->{alias})) {
            my $astrid_tag = $self->find_tag_by_name($tag->{name});

            die 'no such tag' unless $astrid_tag;

            $tag = $self->build_tag($astrid_tag);
        }

        if ($tag) {
            $result{tag_id} = $tag->id_for($self->{alias});
        }
    }

    return \%result;
}

sub find_tag_by_name {
    my $self = shift;
    my $name = shift;

    $self->load_tags();

    return first { $_->{name} eq $name } @{ $self->{tags} };
}

sub find_task {
    my $self = shift;
    my $task = shift;

    $self->load_tasks();

    return first { $_->{created_at} eq to_local_time($task->created_at) } @{ $self->{tasks} };
}

sub update {
    my $self = shift;
    my $task = shift;

    my $astrid_task = $self->find_task($task);

    my $new_task = $self->task_to_hash($task);

    if ($astrid_task) {
        return 0; #don't add tasks that already exists
    }

    my $response = $self->{astrid}->task_save(%$new_task);

    if ($response->{status} eq 'success') {
        debug 'updating task #' . $response->{id};

        $task->ids()->{$self->{alias}} = $response->{id};

        return 1;
    } else {
        error 'cannot update ', $task, $response;
        return 0;
    }
}

1;

