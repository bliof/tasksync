#===============================================================================
#
#         FILE: TaskwarriorProvider.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-16 15:42
#     REVISION: ---
#===============================================================================

package App::Tasksync::Plugin::TaskwarriorProvider;

use strict;
use warnings;

use JSON qw(from_json to_json);
use App::Tasksync::Task;
use App::Tasksync::Logger;
use POSIX qw(strftime mktime);
use List::MoreUtils qw(uniq);
use List::Util qw(first);
use File::Temp qw(tempfile);

sub new {
    my $class = shift;
    my $config = shift;
    my $self = {
        command => $config->{command} || 'task',
        alias => $config->{alias} || 'task'
    };
    bless ($self, $class);
    return $self;
}

sub alias {
    my $self = shift;
    if (ref $self) {
        return $self->{alias};
    }
    return 'task';
}

sub fetch_tasks {
    my $self = shift;
    my @options = @_;

    my $command = $self->{command};

    my $filters = join ' ', @options;

    if ($filters) {
        $command .= " $filters";
    }

    my $json = `$command export`;

    return undef unless $json;

    my $result = from_json("[$json]");

    return $result;
}

sub get_tasks {
    my $self = shift;

    my $tasks = $self->fetch_tasks('end:""');
    my $current = 0;

    return sub {
        if ($tasks->[$current]) {
            my $result = $self->build_task($tasks->[$current]);
            $current++;
            return $result;
        }
        return undef;
    };
}

sub build_task {
    my $self = shift;
    my $task = shift;

    my @tags = ();

    push @tags, $task->{project}   if $task->{project};
    push @tags, @{ $task->{tags} } if $task->{tags};

    @tags = map { App::Tasksync::Task::Tag->new({name => $_}) } uniq @tags;

    my $description = join "\n",
        map { strftime('%F %T', localtime $self->parse_warrior_date($_->{entry})) . ' ' . $_->{description} }
        @{ $task->{annotations} || [] };

    my $internal_task = App::Tasksync::Task->new(
        {
            ids         => { $self->{alias} => $task->{uuid} },
            title       => $task->{description},
            description => $description,
            created_at  => $self->parse_warrior_date($task->{entry}),
            due_date    => $self->parse_warrior_date($task->{due}),
            tags        => \@tags,
            importance  => $self->priority_to_importance($task->{priority})
        }
    );
}

sub format_warrior_date {
    my $self = shift;
    my $date = shift;

    return strftime('%Y%m%dT%H%M%SZ', localtime $date);
}

sub parse_warrior_date {
    my $self = shift;
    my $date = shift or return undef;

    if ($date =~ /(\d\d\d\d)(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)Z/) {
        return mktime($6, $5, $4, $3, $2 - 1, $1 - 1900);
    }

    return undef;
}

sub priority_to_importance {
    my $self = shift;
    my $priority = shift;

    if (defined $priority and $priority =~ tr/HML/321/) {
        return $priority;
    }

    return undef;
}

sub importance_to_priority {
    my $self = shift;
    my $importance = shift;

    if (defined $importance and $importance =~ tr/321/HML/) {
        return $importance;
    }

    return undef;
}

sub find_task {
    my $self = shift;
    my $task = shift;

    if ($task->created_at) {
        my $tasks = $self->fetch_tasks('entry:' . $self->format_warrior_date($task->created_at));
        if ($tasks and @$tasks) {
            return $tasks->[0];
        }
    }

    return 0;
}

sub to_warrior_task {
    my $self = shift;
    my $task = shift;

    my $result = {
        description => $task->title,
        entry => $self->format_warrior_date($task->created_at),
        due => $self->format_warrior_date($task->due_date),
        priority => $self->importance_to_priority($task->importance),
        %{$self->process_tasksync_task_tags($task->tags)}
    };

    return $result;
}

sub process_tasksync_task_tags {
    my $self = shift;
    my $tags = shift;

    unless (defined $tags and @$tags) {
        return {};
    }

    my @task_tags = map { $_->name } @$tags;

    my %projects = map { $_ => 1 } $self->get_projects();
    my %tags     = map { $_ => 1 } $self->get_tags();

    my $project = first { $projects{$_} } @task_tags;

    my @tags;
    if ($project) {
        @tags = grep { $project ne $_ } @task_tags;
    } else {
        $project = shift @task_tags;
        @tags = @task_tags;
    }

    return { project => $project, tags => \@tags };
}

sub get_projects {
    my $self = shift;

    my @result = `$self->{command} _projects`;

    return @result;
}

sub get_tags {
    my $self = shift;

    my @result = `$self->{command} _tags`;

    return @result;
}

sub update {
    my $self = shift;
    my $task = shift;

    return 0 if $self->find_task($task);

    my $command = $self->{command};

    my $data = $self->to_warrior_task($task);
    debug 'saving ', $data;

    my $json = to_json($data);

    my ($fh, $filename) = tempfile();

    print $fh $json;

    `$command import $filename`;

    return $? eq '0';
}

1;

