#===============================================================================
#
#         FILE: Tag.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-12 23:44
#     REVISION: ---
#===============================================================================

package App::Tasksync::Task::Tag;

use strict;
use warnings;

use Class::Accessor 'moose-like';

has ids  => (is => 'rw');
has name => (is => 'rw');

#sub new {
    #my $class = shift;
    #return bless { @_ }, $class;
#}

sub id_for {
    my $self     = shift;
    my $provider = shift;

    return unless $self->ids;

    return $self->ids->{$provider};
}

1;

