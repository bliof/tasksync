#===============================================================================
#
#         FILE: Utils.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-02-11 23:06
#     REVISION: ---
#===============================================================================

package App::Tasksync::Utils;

use strict;
use warnings;

use Exporter;
our @ISA = qw/Exporter/;
our @EXPORT_OK = qw(
    format_plugin_name
);

sub format_plugin_name {
    my $class = shift or return '';

    if ($class =~ s/^\+//) {
        return $class;
    }

    if ($class =~ /^App::Tasksync::Plugin::/) {
        return $class;
    }

    return 'App::Tasksync::Plugin::' . $class;
}

1;

