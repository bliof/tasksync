#===============================================================================
#
#         FILE: Tasksync.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Aleksandar Ivanov, aivanov92@gmail.com
# ORGANIZATION:
#      VERSION: 0.1
#      CREATED: 2013-01-05 14:43
#     REVISION: ---
#===============================================================================

package App::Tasksync;

use strict;
use warnings;

use File::HomeDir qw(home);
use File::Spec::Functions qw(catfile);
use YAML qw(LoadFile);
use Class::Accessor 'moose-like';
use Module::Pluggable;
use Module::Load;
use List::Util qw(first);
use App::Tasksync::Utils qw(format_plugin_name);
use App::Tasksync::Logger;

has config => (is => 'rw');

sub new {
    my $class = shift;
    my %options = @_;

    my $self = {
        config => $options{config},
    };
    bless ($self, $class);

    unless ($self->{config}) {
        $self->load_config($options{config_file});
    }

    $self->{config} ||= {};
    $self->{config}->{providers} ||= {};

    #holds the loaded providers
    $self->{providers} = {};

    for my $provider ($self->plugins) {
        load $provider;
    }

    return $self;
}

sub load_config {
    my $self = shift;
    my $config_file = shift || catfile(home(), '.tasksyncrc');

    if ($config_file and -e $config_file) {
        $self->{config_file} = $config_file;

        debug "loading config [$config_file]";
        my $config = LoadFile($config_file);

        $self->{config} = $config;

        return 1;
    }
    debug "cannot load config [$config_file]";

    $self->{config} = {};
    return 0;
}

sub get_provider {
    my $self = shift;
    my $name = shift;

    unless ($self->{providers}->{$name}) {
        my $provider_class = $self->_find_provider_class($name);

        die "cannot find provider with name = $name" unless $provider_class;

        load $provider_class;

        $self->{config}->{providers}->{$name} ||= {};
        $self->{config}->{providers}->{$name}->{alias} = $name;

        my $provider = $provider_class->new($self->{config}->{providers}->{$name});

        $self->{providers}->{$name} = $provider;
    }

    return $self->{providers}->{$name};
}

sub _find_provider_class {
    my $self = shift;
    my $name = shift;

    my $config = $self->{config}->{providers}->{$name} || {};
    my $class = format_plugin_name $config->{class};

    return $class if $class;

    my @plugins = $self->plugins();

    $class = first { $name eq $_->alias() } grep { $_->can('alias') } @plugins;

    return $class;
}

sub sync {
    my $self = shift;
    my %params = @_;
    my $from = $params{from};
    my $to = $params{to};

    if ($from eq $to) {
        return 0;
    }

    debug "sync from $from to $to";

    my $provider = $self->get_provider($from);
    my $receiver = $self->get_provider($to);

    my $tasks = $provider->get_tasks();

    my $next_task = _build_tasks_iterator($tasks);

    my $processed = 0;
    while (my $current = $next_task->()) {
        debug $current;
        if ($receiver->update($current)) {
            $processed++;
        }
    }

    debug "total task synced [$processed]";

    return $processed;
}

sub has_tasks_to_sync {
    my $self = shift;
    my %params = @_;
    my $from = $params{from};
    my $to = $params{to};

    if ($from eq $to) {
        return 0;
    }

    my $provider = $self->get_provider($from);
    my $receiver = $self->get_provider($to);

    my $from_task_it = _build_tasks_iterator($provider->get_tasks());
    my $to_task_it = _build_tasks_iterator($receiver->get_tasks());

    my %to_tasks_dates;

    while (my $task = $to_task_it->()) {
        $to_tasks_dates{$task->created_at} = 1;
    }

    while (my $from_task = $from_task_it->()) {
        return 1 unless $to_tasks_dates{$from_task->created_at};
    }

    return 0;
}

#===============================================================================
# random util function
#===============================================================================

sub _build_tasks_iterator {
    my $tasks = shift;

    my $next_task;

    if (ref $tasks eq 'ARRAY') {
        my $current = 0;
        $next_task = sub {
            my $task = $tasks->[$current];

            $current++ if $task;
            return $task;
        };
    } elsif (ref $tasks eq 'CODE') {
        $next_task = $tasks;
    } else {
        die 'Cannot build iterator for: ' . ref($tasks);
    }

    return $next_task;
}

1;

